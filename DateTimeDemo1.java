import java.text.SimpleDateFormat;
import java.util.Date;
public class DateTimeDemo1 {
	public static void main(String[] args) {
		Date today = new Date();
		
		System.out.println(today);
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		System.out.println(sdf.format(today));
		
	}
}
